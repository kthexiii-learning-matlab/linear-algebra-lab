%  la_labb02.m
%
%  Created by Pratchaya Khansomboon on 2019-12-11.
%  Copyright © 2019 Pratchaya Khansomboon. All rights reserved.

clc
clear
close all
format short

% / / / / / / / / / / / / / Uppgift 01 / / / / / / / / / / / / /
fprintf('Uppgift 1\n')
A01 = [
    1 1 1
    1 2 5
    1 7 2
    ];

b01 = [
    0
    1
    -1
    ];

format short
x01short = A01 \ b01
format rat
x01rat = A01 \ b01

% / / / / / / / / / / / / / Uppgift 02 / / / / / / / / / / / / /
t02 = -1:0.1:1;
% s02 = t02;
[S02, T02] = meshgrid(t02, t02);
% Plane 1
X102 = 1 + 2 .* S02 + 2 .* T02;
Y102 = 2 - S02 - 2 .* T02;
Z102 = 1 + S02 + T02;
% Plane 2
X202 = 3 + S02 + T02;
Y202 = 1 - 2 .* S02 + T02;
Z202 = 2 + S02 - T02;

figure
plot3(X102, Y102, Z102, X202, Y202, Z202)
grid on
title('Uppgift 2')
xlabel('x')
ylabel('y')
zlabel('z')

figure
mesh(X102, Y102, Z102)
hold on
mesh(X202, Y202, Z202)
hold off
grid on
title('Uppgift 2')
xlabel('x')
ylabel('y')
zlabel('z')

% / / / / / / / / / / / / / Uppgift 03 / / / / / / / / / / / / /
t103 = -10:0.1:10;
t203 = -5:0.1:5;
% Line 1
line103(:, 1) = 1 - 2 .* t103;
line103(:, 2) = 2 + t103;
line103(:, 3) = -1 + 4 .* t103;
% Line 2
line203(:, 1) = 1 - 4 .* t203;
line203(:, 2) = 4 .* t203;
line203(:, 3) = 2 + 5 .* t203;

figure
plot3(line103(:, 1), line103(:, 2), line103(:, 3), line203(:, 1), line203(:, 2), line203(:, 3), 'linewidth', 2)
grid on
title('Uppgift 3')
xlabel('x')
ylabel('y')
zlabel('z')

% / / / / / / / / / / / / / Uppgift 04 / / / / / / / / / / / / /
fprintf('Uppgift 4\n')
u04 = [1 -2 3];
v04 = [2 3 1];

% cross product
cpuv = cross(u04, v04);
cpvu = cross(v04, u04);

fprintf('(u x v):\n\t(%d, %d, %d)\n', cpuv)
fprintf('(v x u):\n\t(%d, %d, %d)\n', cpvu)

% / / / / / / / / / / / / / Uppgift 05 / / / / / / / / / / / / /
fprintf('\nUppgift 5\n')
A05 = [1 2 1];
B05 = [3 -2 1];
C05 = [1 -2 0];

AB05 = B05 - A05;
AC05 = C05 - A05;
n05 = cross(AB05, AC05);

fprintf('normal vector: (%d, %d, %d)\n', n05)

% / / / / / / / / / / / / / Uppgift 06 / / / / / / / / / / / / /
fprintf('\nUppgift 6\n')
% Point
P06 = [1 -2 3];

% 3 point on a plane
A06 = [-1 -3 -4];
B06 = [-5 -5 1];
C06 = [3 1 2];

% Calculate normal vector
AB06 = B06 - A06;
AC06 = C06 - A06;
n06 = cross(AB06, AC06)

% format short
u06 = P06 - A06;
up06 = (dot(u06, n06) / dot(n06, n06)) * n06;
Q06 = P06 - up06
S06 = P06 - (2 * up06)

t06 = -3:0.5:1;
[MS06, MT06] = meshgrid(t06);
plEq = @(x, y) - 4 .* x + 5.5 .* y + 8.5;

figure
mesh(MS06, MT06, plEq(MS06, MT06))
hold on
plot3(P06(1), P06(2), P06(3), 'b.', 'MarkerSize', 20)
plot3(Q06(1), Q06(2), Q06(3), 'c.', 'MarkerSize', 20)
plot3(S06(1), S06(2), S06(3), 'r.', 'MarkerSize', 20)
hold off

title('Uppgift 6')
xlabel('x')
ylabel('y')
zlabel('z')

% / / / / / / / / / / / / / Uppgift 07 / / / / / / / / / / / / /
fprintf('\nUppgift 7\n')
P07 = [1 2 3];
PL07EQ = @(x, y) 2 .* x + y + 2;
n07 = [2 1 -1];

A07 = [1 1 PL07EQ(1, 1)];
u07 = P07 - A07;
up = (dot(u07, n07) / dot(n07, n07)) * n07;
d = sqrt(dot(up, up))

% d07 = abs(2 * 1 + 1 * 2 + (-1) * 3 + 2) / sqrt(2^2 + 1^2 + (-1)^2)
