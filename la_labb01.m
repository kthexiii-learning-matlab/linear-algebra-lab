%  la_labb01.m
%
%  Created by Pratchaya Khansomboon on 2019-12-11.
%  Copyright © 2019 Pratchaya Khansomboon. All rights reserved.

clc
clear
close all
format short

% / / / / / / / / / / / / / Uppgift 01 / / / / / / / / / / / / /
a01 = 35 * 2^5;
b01 = 5^(1/2) + 1/4;
c01 = exp(10);
d01 = tan(pi / 6);

fprintf('Uppgift 1\n')
fprintf('\ta: %d\n', a01)
fprintf('\tb: %d\n', b01)
fprintf('\tc: %d\n', c01)
fprintf('\td: %d\n', d01)

% / / / / / / / / / / / / / Uppgift 02 / / / / / / / / / / / / /
a02 = 1.3533 * 10^(16);
b02 = 2.1191 * 10^(-11);

fprintf('\nUppgift 2\n')
fprintf('\ta: %s\n', num2str(a02))
fprintf('\tb: %s\n', num2str(b02))

% / / / / / / / / / / / / / Uppgift 03 / / / / / / / / / / / / /
x03 = 0:0.1:10;
y03(:, 1) = sin(x03);
y03(:, 2) = sin(x03) .* cos(x03);

figure
plot(x03, y03)
title('Uppgift 3')

% / / / / / / / / / / / / / Uppgift 04 / / / / / / / / / / / / /
fprintf('\nUppgift 4')
a04 = 10:2:32
b04 = -2:-3:-35

% / / / / / / / / / / / / / Uppgift 05 / / / / / / / / / / / / /
fprintf('\nUppgift 5')
f05 = @(x) x .* 1.2 + x.^(1/2) - 2;
x05 = 0:0.01:2;

figure
plot(x05, f05(x05))
grid on
title('Uppgift 5')
xlabel('x')
ylabel('y')
fz05 = fzero(f05, 1)

% / / / / / / / / / / / / / Uppgift 06 / / / / / / / / / / / / /
fprintf('\nUppgift 6')
f06 = @(x) exp(-x) .* sin(x);
x06 = 0:0.01:6;

figure
plot(x06, f06(x06))
grid on
title('Uppgift 6')
xlabel('x')
ylabel('y')

fmin = fminsearch(f06, 4)

% / / / / / / / / / / / / / Uppgift 07 / / / / / / / / / / / / /
fprintf('\nUppgift 7\n')
f07 = @(x) x.^3 + 2 .* x.^2 - x + 1;
i07 = integral(f07, 0, 5);

fprintf('Integral 0 → 5: %s\n', num2str(i07))
